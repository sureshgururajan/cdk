import '@aws-cdk/assert/jest';
import * as cdk from '@aws-cdk/core';
import * as Cdk from '../lib/cdk-stack';

test('Empty Stack', () => {
  const app = new cdk.App();
    // WHEN
    const stack = new Cdk.SureshCDKStack(app, 'MyTestStack');
    // THEN
    expect(stack).toHaveResource('AWS::S3::Bucket');
    expect(stack).toHaveResource('AWS::Lambda::Function');
});
